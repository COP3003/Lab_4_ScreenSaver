/**
 * FileName: ScreenSaver.java 
 * Author: 5153
 * Created: October 31, 2017
 * Last Modified: November 1, 2017
 */

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JPanel;
import javax.swing.Timer;
/**
 * Repeatedly draws a spiral in a window.
 * <br />
 * This lab assignment is a small graphics program that can draw a growing
 * spiral. Each ring on the spiral is composed of 6 straight edges. The spiral
 * grows one edge every some time. Initially, the frequency is one edge per
 * second. It can be changed by using your mouse wheel. In detail, when you are
 * scrolling up, the growing gets faster; vice versa. Once the spiral grows to
 * 10 rings, the whole spiral will be erased from the panel and be redrawn all
 * over again.
 * <br />
 * @author 5153
 */
public class ScreenSaver extends JPanel 
                        implements ActionListener, MouseWheelListener{
  private int x[]=new int[60]; // each ring is defined by 10 points, then
  private int y[]=new int[60]; // 10 rings would be 60 points
  private int numOfPoints=0;// how many have been generated?
  private int radius=10;
  private int delay=1000;
  private Timer timer=null; // javax.swing.timer
  
  /**
   * Starts the draw timer and mouse wheel listener
   */
  public ScreenSaver(){
    timer=new Timer(1000, this); // the interval is 1000 milliseconds
    timer.start();
    addMouseWheelListener(this);
  }
  
  /**
   * This is executed every time the timer triggers. The line is extended
   *  one segment and the entire surface is redrawn.
   */
  public void actionPerformed(ActionEvent e){
    // Finds the origin 
    double originX=getSize(null).getWidth()/2;
    double originY=getSize(null).getHeight()/2;
    
    //generate an x,y point
    double pX = originX + Math.cos(numOfPoints * Math.PI/3)*radius;
    double pY = originY + Math.sin(numOfPoints * Math.PI/3)*radius;
 
    // add the point to x[],y[] in the order of creation 
    x[numOfPoints] = (int)pX;
    y[numOfPoints] = (int)pY;
    
    // increment point counter up by one or reset to 0
    numOfPoints = (numOfPoints + 1) % 60;
    
    // Resets the radius and numOfPoints
    radius = (numOfPoints==0)?10:radius + 3;

    repaint();  // causes the entire component to be repainted
  }
  
  /**
   * Draws the segmented line on the surface.
   */
  public void paintComponent(Graphics g){
    // this clears the window then draws a rectangle on the 
    //    container from 0,0 to the containers width, height
    g.clearRect(0,0,(int)getSize().getWidth(),(int)getSize().getHeight());

    // draws a line from the first point to each succeeding point
    g.drawPolyline(x, y, numOfPoints);
  }
  
  @Override
  public void mouseWheelMoved(MouseWheelEvent e){
  /*
   * e.getWheelRotation() returns the number of "clicks" the mouse wheel was 
   * rotated. Negative values if the mouse wheel rotates up/away from the 
   * user, & positive values if the mouse wheel rotates down/towards the user
   */
    
    // get direction of angular velocity (w = small omega)
    int w = e.getWheelRotation();
    
    // Rotating the wheel towards you slows the draw speed.
    int direction = (w > 0)? 1: -1;
    
    // Scaling factor: the value of each click of the wheel changes based on
    //  the current delay. The lower the delay, the smaller the value.
    int factor = (int) Math.pow(Math.E,Math.log10(delay)) * 2 * direction;
    
    // Makes sure the delay cannot equal zero.
    timer.setDelay(delay += (delay + factor > 0)? factor : 0);
    
    // Restarts the Timer, canceling any pending firings and causing it to fire
    //  with the above delay.
    timer.restart();    
  }
}